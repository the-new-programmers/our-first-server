const express = require('express');
const server = express();
require('dotenv').config();
const port = process.env.PORT;
const fs = require('fs');


// bodyParser
const bodyParser = require('body-parser');
// server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }))


// static files
server.use('/app-client', express.static('app-client'))
// server.use(express.static('public'));
/*server.get('/some-image.png', function(req, res) {
    res.sendFile(path.join(__dirname + '/first-step.js'));
});*/



server.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});

require('./routers.js')(server);



/*
 fs.readFile('', (err, data) => {});
 fs.readFile('', '', (err, data) => {});
 let statisticsFileData = fs.readFile('./statistics', 'utf8', (err, data) => {
     data;
 });
 statisticsFileData // undefined
 */

function readFilePromise() {
   return new Promise((resolve, reject) => {
        fs.readFile('./statistics', 'utf8', (err, data) => {
            if (err) {
                reject('ERROR' + err)
            }

            resolve(data);
        });
    });
}

// statistics :-)
async function statisticsAddOne() {
    let statisticsFileData = await readFilePromise();
    fs.writeFile('./statistics', +statisticsFileData + 1, () => {});
}

