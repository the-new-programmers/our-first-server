const path = require('path');
const fs = require('fs');

// upload files
const multer = require('multer');
const uploadAvatar = multer({ dest: './app-client/uploads/avatar' } );
// const upload2 = multer({ dest: './app-client/uploads/images' } );


module.exports = (server) => {
    // home router -> GET
    server.get('/', function(req, res) {

        statisticsAddOne();

        res.sendFile(path.join(__dirname + '/app-client/html/index.html'));
    });

// login router -> GET
    server.get('/login', (req, res, next) => {
        res.sendFile(path.join(__dirname + '/app-client/html/login.html'));
    });

// login router -> POST
    server.post('/login', (req, res, next) => {

        if (req.body.password === process.env.APP_PASSWORD) {
            return res.redirect('/');
        }

        res.sendFile(path.join(__dirname + '/app-client/html/login.html'));
    });



// profile router -> GET
    server.get('/profile', (req, res, next) => {
        res.sendFile(path.join(__dirname + '/app-client/html/profile.html'));
    });

// profile router -> POST
    server.post('/profile', uploadAvatar.single('avatar'), (req, res, next) => {

        const uploadAvaterPath = __dirname + '/app-client/uploads/avatar/';
        const oldFile = uploadAvaterPath + req.file.filename;
        const newFile = uploadAvaterPath + req.file.filename + '.' + (req.file.originalname.split('.').pop());

        fs.rename(oldFile, newFile, (err) => {
            if (err) throw err;
        });


        res.sendFile(path.join(__dirname + '/app-client/html/profile.html'));
    });
}
