
const moveSnakeInterval = setInterval(() => {
    move();
}, 100);


document.addEventListener('keydown', (e) => {

    switch (e.code) {
        case 'ArrowUp':
            move('up');
            break;
        case 'ArrowDown':
            move('down');
            break;
        case 'ArrowLeft':
            move('left');
            break;
        case 'ArrowRight':
            move('right');
            break;
    }
});


function move(clickDir = 'keep') {

    // calculate direction snake
    let new_x;
    let new_y;
    const calcDirIsUp = snake[snake.length - 1].x < snake[snake.length - 2].x;
    const calcDirIsDown = snake[snake.length - 1].x > snake[snake.length - 2].x;
    const calcDirIsLeft = snake[snake.length - 1].y < snake[snake.length - 2].y;
    const calcDirIsRight = snake[snake.length - 1].y > snake[snake.length - 2].y;


    if ((clickDir === 'up' && !calcDirIsDown) || (clickDir === 'keep' && calcDirIsUp)) {
        // up
        new_x = snake[snake.length - 1].x - 1;
        new_y = snake[snake.length - 1].y;
    }
    else if ((clickDir === 'down' && !calcDirIsUp) || (clickDir === 'keep' && calcDirIsDown)) {
        // down
        new_x = snake[snake.length - 1].x + 1;
        new_y = snake[snake.length - 1].y;
    }
    else if ((clickDir === 'left' && !calcDirIsRight) || (clickDir === 'keep' && calcDirIsLeft)) {
        // left
        new_x = snake[snake.length - 1].x;
        new_y = snake[snake.length - 1].y - 1;
    }
    else if ((clickDir === 'right' && !calcDirIsLeft) || (clickDir === 'keep' && calcDirIsRight)) {
        // right
        new_x = snake[snake.length - 1].x;
        new_y = snake[snake.length - 1].y + 1;
    }

    if (new_x !== undefined && new_y !== undefined) {

        // update snake and bord
        snake.push({
            x: new_x,
            y: new_y
        });

        // check if the user is failed
        if (
               snake[snake.length - 1].x < 0
            || snake[snake.length - 1].x > bordWidth
            || snake[snake.length - 1].y < 0
            || snake[snake.length - 1].y > bordWidth
            || bord[new_x][new_y].val === 'snake'
        ) {
            clearInterval(moveSnakeInterval);
            alert('נפסלת יא לוזר');
        }
        else {

            if (bord[new_x][new_y].val === 'food') {
                setFood();
            }
            else {
                // cut snake tail
                bord[snake[0].x][snake[0].y] = { val: '' };
                snake.shift();
            }



            bord[new_x][new_y] = {val: 'snake'};

            // draw HTML
            drawHtmlBord();
        }
    }
}

function setFood() {
    let x = randomNimMax(0, bordWidth - 1);
    let y = randomNimMax(0, bordWidth - 1);

    while (bord[x][y].val === 'snake') {
        x = randomNimMax(0, bordWidth - 1);
        y = randomNimMax(0, bordWidth - 1);
    }

    bord[x][y] = { val: 'food' };
}




