let htmlBordElementRef;
const bord = [];
const snake = [];
const bordWidth = 40;

window.addEventListener('load', () => {

// create the js bord object
    for (let i = 0; i < bordWidth; i++) {
        bord[i] = [];
        for (let j = 0; j < bordWidth; j++) {
            bord[i].push({});
        }
    }

// add the snake to the bord
    bord[bordWidth / 2]      [bordWidth / 2]  = {val: 'snake'};
    bord[(bordWidth / 2) - 1][bordWidth / 2]  = {val: 'snake'};
    bord[(bordWidth / 2) - 2][bordWidth / 2]  = {val: 'snake'};

    snake.push( {x: bordWidth / 2,          y: bordWidth / 2} );
    snake.push( {x: (bordWidth / 2) - 1,    y: bordWidth / 2} );
    snake.push( {x: (bordWidth / 2) - 2,    y: bordWidth / 2} );


    htmlBordElementRef = document.querySelector('#bord');

    setFood();
    drawHtmlBord();
});


function drawHtmlBord() {
    // reset html bord!
    htmlBordElementRef.innerHTML = '';

    // create html bord
    for (const keyRow in bord) {

        let rowHtmlElement = document.createElement("div");
        rowHtmlElement.setAttribute("id", `row-${keyRow}`);
        rowHtmlElement.setAttribute("class", `row`);
        htmlBordElementRef.appendChild(rowHtmlElement);

        for (const keyCell in bord[keyRow]) {
            let cellHtmlElement = document.createElement("span");
            cellHtmlElement.setAttribute("id", `row-${keyRow}-cell-${keyCell}`);
            cellHtmlElement.setAttribute("class", `cell`);

            if (bord[keyRow][keyCell].val === 'snake') {
                cellHtmlElement.setAttribute("class",
                    +keyRow === +snake[snake.length -1].x && +keyCell === +snake[snake.length -1].y ? `cell snake-head` : `cell snake`
                );
            }

            if (bord[keyRow][keyCell].val === 'food') {
                cellHtmlElement.setAttribute("class",`cell food`);
            }

            rowHtmlElement.appendChild(cellHtmlElement);
        }
    }
}
